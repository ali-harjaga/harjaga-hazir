<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->time('check_in')->nullable();
            $table->time('check_out')->nullable();
            $table->date('date')->nullable();
            $table->boolean('is_absent')->default(false);
            $table->text('checkin_location')->nullable();
            $table->text('checkout_location')->nullable();
            $table->string('check_in_pic')->nullable();
            $table->string('check_out_pic')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
