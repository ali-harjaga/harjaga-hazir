<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ShiftFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->randomElement(['morning', 'evening', 'night']);
        return [
            'name' => $name,
            'shift_start' => $this->faker->dateTime($max = 'now'),
            'shift_end' => $this->faker->dateTime($max = 'now'),
            'days' => $this->faker->dayOfWeek($max = 'now'),
        ];
    }
}
