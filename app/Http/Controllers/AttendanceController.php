<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper;
use App\Models\Attendance;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    // <!--------------------------------------------  CheckIn method   -----------------------------------------------------------------!>

    public function checkIn(Request $request)
    {
        // return $request->allFiles();
        $validation = $this->validateUser($request);

        if ($validation->fails()) {
            $result = ApiHelper::validation_error('Validation Error', $validation->errors());
            return response()->json($result, 422);
        }

        if ($this->userCheckedIn($request->user_id)) {
            $result = ApiHelper::error('User has already checked in');
            return response()->json($result, 404);
        };

        $picture_name = $this->uploadPicture($request, 'check_in_pic', 'check_in');

        $data = Attendance::create([
            'user_id' => $request->user_id,
            'check_in' => $this->currentTime(),
            'date' => $this->today(),
            'checkin_location' => $request->checkin_location,
            'check_in_pic' => $picture_name,
            'status' => 1,
        ]);

        $result = ApiHelper::success('User check in Successfully', $data);
        return response()->json($result, 200);
    }
    // <!--------------------------------------------  CheckOut method   -----------------------------------------------------------------!>
    public function checkOut(Request $request)
    {
        $validation = $this->validateUser($request);
        if ($validation->fails()) {
            $result = ApiHelper::validation_error('Validation Error', $validation->errors());
            return response()->json($result, 422);
        }

        $user_id = $request->user_id;


        if (!$this->userCheckedIn($user_id)) {
            $result = ApiHelper::error('You must check in first!');
            return response()->json($result, 404);
        } else if ($this->userCheckedOut($user_id)) {
            $result = ApiHelper::error('User has already checked out');
            return response()->json($result, 404);
        }

        $picture_name = $this->uploadPicture($request, 'check_in_pic', 'check_out');

        $data = $this->userExists($user_id)->update([
            'check_out' => $this->currentTime(),
            'checkout_location' => $request->checkin_location,
            'check_out_pic' => $picture_name,
        ]);

        $result = ApiHelper::success('User check out Successfully', $data);
        return response()->json($result, 200);
    }
    // <!--------------------------------------------  Upload image for checkin and checkout   -----------------------------------------------------------------!>


    public function uploadPicture(object $request, string $picturename, string $type): string
    {
        if ($request->hasFile($picturename)) {
            $extension = $request->file($picturename)->extension();
            $picture_name = $type . '_' . $this->today() . '.' . $extension;
            // dd($picture_name);
            // $directory = 'attendance/' . $this->today() . '/' . $request->user_id;
            // Storage::disk('s3')->makeDirectory($directory);
            $filePath = 'attendance/' . $this->today() . '/' . $request->user_id;
            $savedImage = Storage::disk('s3')->put($filePath, $request->file($picturename));
            $savedImageName = explode("/", $savedImage);
            // $request->file($picturename)->move(public_path('uploads/images/' . $this->today() . '/' . $request->user_id . '/'), $picture_name);
        } else {
            $picture_name = '';
            $$savedImageName = '';
        }
        return $savedImageName[3];
    }


    // <!--------------------------------------------  Validate user   -----------------------------------------------------------------!>

    public function validateUser($request)
    {
        $validation = Validator::make($request->all(), [
            'user_id' => ['required'],
        ]);
        return $validation;
    }

    // <!--------------------------------------------  Check if user exist for today   -----------------------------------------------------------------!>

    public function userExists($user_id)
    {
        $attendance = Attendance::where(['user_id' => $user_id, 'date' => $this->today()]);
        return $attendance;
    }

    // <!--------------------------------------------  check if user checked in method   -----------------------------------------------------------------!>

    public function userCheckedIn(int $user_id): bool
    {
        $exist = $this->userExists($user_id)->whereNotNull('check_in')->exists();
        return $exist;
    }

    // <!--------------------------------------------  check if user checked out method   -----------------------------------------------------------------!>

    public function userCheckedOut($user_id): bool
    {
        $exist = $this->userExists($user_id)->whereNotNull('check_out')->exists();
        return $exist;
    }

    // <!--------------------------------------------  time method   -----------------------------------------------------------------!>

    public function currentTime()
    {
        $datetime = new DateTime();
        $time = $datetime->format('Y-m-d H:i:s');
        return $time;
    }

    // <!--------------------------------------------  today date method   -----------------------------------------------------------------!>
    public function today()
    {
        $datetime = new DateTime();
        $today = $datetime->format('Y-m-d');
        return $today;
    }

    // <!--------------------------------------------  User Status Method   -----------------------------------------------------------------!>

    public function userStatus(Request $request)
    {
        if ($request->user_id) {
            $checked_in = false;
            $checked_out = false;
            $check_attendance = Attendance::where('user_id', $request->user_id)->where('date', $this->today())->first();
            if (!$check_attendance) {
                $data['checked_in'] = $checked_in;
                $data['checked_out'] = $checked_out;
            } else {
                if ($check_attendance->check_in) {
                    $checked_in = true;
                }
                if ($check_attendance->check_out) {
                    $checked_out = true;
                }
            }
            $data['checked_in'] = $checked_in;
            $data['checked_out'] = $checked_out;
            $result = ApiHelper::success('User Status', $data);
            return response()->json($result, 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
}
