<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper;
use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            Auth::logout();
            $loginData = Validator::make($request->all(), [
                'email' => ['required', 'email'],
                'password' => ['required']
            ]);

            if ($loginData->fails()) {
                $result = ApiHelper::validation_error('Validation Error', $loginData->errors());
                return response()->json($result, 422);
            }
            if (!auth()->attempt($request->only(['email', 'password']))) {
                $result = ApiHelper::validation_error('Validation Error', ['Invalid Credentials']);
                // $result = ApiHelper::error('Invalid Credentials');
                return response()->json($result, 422);
            }
            // else if(auth()->attempt($request->only(['email', 'password'])) && auth()->user()->is_active == 0){
            //     $result = ApiHelper::unprocessable_entity('Inactive User Error');
            //     return response()->json($result, 403);
            // }
            // $accessToken = auth()->user()->createToken(config('main.secret'))->accessToken;

            $loginUser['user'] = auth()->user();

            // if($loginUser['user']->image_updated == 1) {
            //     $loginUser['user']->generic1 = config('main.aws-cdn') . 'profiles/' . $loginUser['user']->generic1;
            // } 
            // $loginUser['access_token'] = $accessToken;
            // $loginUser['isAgent'] = (auth()->user()->isAgent == null) ? false : true;
            $loginUser['process'] = "Login";
            $result = ApiHelper::success('Login Successfully', $loginUser);
            return response()->json($result, 200);
        } catch (Exception $e) {
            $result = ApiHelper::validation_error('Exception Error', $e);
            return response()->json($result, 400);
        }
    }

    public function forgotPassword(Request $request)
    {
        $passwordRequest = Validator::make($request->all(), [
            'email' => 'required|string|email'
        ]);

        if ($passwordRequest->fails()) {
            $result = ApiHelper::validation_error('Validation Error', $passwordRequest->errors());
            return response()->json($result, 422);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $result = ApiHelper::error('User with this email address not found');
            return response()->json($result, 400);
        }
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
            ]
        );
        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        $result = ApiHelper::successMessage('We have e-mailed your password reset link!');
        return response()->json($result, 200);
    }

    public function reset(Request $request)
    {

        $passwordRequest = Validator::make($request->all(), [
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        if ($passwordRequest->fails()) {
            $result = ApiHelper::validation_error('Validation Error', $passwordRequest->errors());
            return response()->json($result, 422);
        }
        $passwordReset = PasswordReset::where([
            ['token', $request->token],

        ])->first();
        if (!$passwordReset){
            $result = ApiHelper::error('This password reset token is invalid');
            return response()->json($result, 404);
        }
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user) {
            $result = ApiHelper::error('User with this email address not found');
            return response()->json($result, 404);
        }
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        $result = ApiHelper::success('Password reset successfully', $user);
            return response()->json($result, 200);
    }
}
