<?php

use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login',[AuthController::class, 'login']);
Route::post('/forgot-password',[AuthController::class, 'forgotPassword']);
Route::post('/reset-password',[AuthController::class, 'reset']);
Route::post('/check-in',[AttendanceController::class, 'checkIn']);
Route::post('/check-out',[AttendanceController::class, 'checkOut']);
Route::get('/user-status',[AttendanceController::class, 'userStatus']);