<?php

return [
    'aws_region' => env('AWS_DEFAULT_REGION'),
    'aws_bucket' => env('AWS_BUCKET'),
    'aws-cdn' => env('AWS_CDN'),
];
