<!DOCTYPE html>
<html>
<head>
    <title>Harjaga.com</title>
    <style>
        .wrapper{
            width: 50%;
            border: 1px solid lightgray;
            margin: 20px auto;
            border-radius: 1rem;
        }
        .upper-div{
            color:white;
            background-color: #1ed0b1;
            text-align: center;
            padding-top: 20px;
            border-top-left-radius: 1rem;
            border-top-right-radius: 1rem;
        }
        .lower-div{
            color: black;
            text-align: center;
            font-size: 14px;
        }
        .lower-div p{
            color: black;
        }
        .reset-btn{
            text-decoration: none;
            color: white !important;
            background: #00cdad;
            padding: 10px;
            border-radius: 5px;
            font-weight: 600;
        }
        .other-link{
            width: 80%;
            margin: 0px auto;
            margin-bottom: 10px;
        }
        @media (max-width: 580px) {
            .wrapper{
                width: 80%;
            }
        }
    </style>
</head>
<body>
    <div style="text-align:center">
        <img src="{{ url('/') }}/images/Harjaga-logo.png" width="200" height="40" style="margin-bottom:10px"/>
    </div>
    <div class="wrapper">
        <div class="upper-div">
            <img src="{{ url('/') }}/images/tick.png" width="50" height="50" />
            <h2>Hello!</h2>
        </div>
        <div class="lower-div">
            <p>You are receiving this email because we received a password reset request for your account.</p>
            <a href={{$url}} class="reset-btn">Reset Password</a>
            <p>If you did not request a password reset, no further action is required.</p>
            <p>Regards,</p>
            <img src="{{ url('/') }}/images/Harjaga-logo.png" width="200" height="40" style="margin-bottom:10px"/>
            <p class="other-link">If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: {{$url}}</p>
        </div>
    </div>
</body>
</html>
