<!DOCTYPE html>
<html>
<head>
    <title>Harjaga.com</title>
    <style>
        .wrapper{
            width: 50%;
            border: 1px solid lightgray;
            margin: 20px auto;
            border-radius: 1rem;
        }
        .upper-div{
            color:white;
            background-color: #1ed0b1;
            text-align: center;
            padding-top: 20px;
            border-top-left-radius: 1rem;
            border-top-right-radius: 1rem;
        }
        .lower-div{
            color: black;
            text-align: center;
            font-size: 14px;
        }
        .lower-div p{
            color: black;
        }
        @media (max-width: 580px) {
            .wrapper{
                width: 80%;
            }
        }
    </style>
</head>
<body>
    <div style="text-align:center">
        <img src="{{ url('/') }}/images/Harjaga-logo.png" width="200" height="40" style="margin-bottom:10px"/>
    </div>
    <div class="wrapper">
        <div class="upper-div">
            <img src="{{ url('/') }}/images/tick.png" width="50" height="50" />
            <h2>CONGRATULATIONS</h2>
        </div>
        <div class="lower-div">
            <h1>Hello!</h1>
            <p>Your password has been changed successfully.</p>
            <p>If you did change your password, no further action is required.</p>
            <p>If you did not change password, protect your account.</p>
            <p>Regards,</p>
            <img src="{{ url('/') }}/images/Harjaga-logo.png" width="200" height="40" style="margin-bottom:10px"/>
        </div>
    </div>
</body>
</html>
